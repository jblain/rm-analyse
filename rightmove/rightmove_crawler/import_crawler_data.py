from .crawler import RightmoveData
from .models import Property


class DataImport:
    def __init__(self, url):
        self.url = url

    def _import_data(self):
        crawler = RightmoveData(self.url.url)
        # Gets a dataframe of the results
        df = crawler.get_results

        # Create list of property objects
        properties = [
            Property(
                search_url=self.url,
                property_url=row.url,
                price=row.price,
                property_type=row.type,
                address=row.address,
                agent_url=row.agent_url,
                postcode=row.postcode,
                number_bedrooms=row.number_bedrooms,
                search_date=row.search_date,
            )
            for row in df.itertuples()
        ]
        Property.objects.bulk_create(properties, batch_size=500)
