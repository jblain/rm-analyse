from django.urls import path

from . import views

urlpatterns = [
    path("list/", views.PropertyListView.as_view(), name="property_list"),
    path("searches/", views.SearchURLListView.as_view(), name="search_list"),
    path("searches/create", views.SearchURLCreateView.as_view(), name="search_create"),
    path("searches/update/<int:pk>", views.SearchURLUpdateView.as_view(), name="search_update"),
]
