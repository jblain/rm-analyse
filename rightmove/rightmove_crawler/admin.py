from django.contrib import admin

from . import models

# Register your models here.
admin.site.register(models.SearchURL)
admin.site.register(models.Property)
