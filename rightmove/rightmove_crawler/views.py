from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views import generic

from .models import Property, SearchURL

# Create your views here.


class UserFilteredListMixin(LoginRequiredMixin, generic.ListView):
    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class SearchURLListView(UserFilteredListMixin):
    model = SearchURL


class SearchURLCreateView(LoginRequiredMixin, generic.edit.CreateView):
    model = SearchURL
    fields = ["url"]
    success_url = "/rightmove_crawler/searches/"

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class SearchURLUpdateView(UserPassesTestMixin, generic.edit.UpdateView):
    model = SearchURL
    fields = ["url"]
    success_url = "/rightmove_crawler/searches/"

    def test_func(self):
        self.object = self.get_object()
        return self.request.user == self.object.user

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class PropertyListView(UserFilteredListMixin):
    model = Property
