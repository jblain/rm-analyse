from django.apps import AppConfig


class RightmoveCrawlerConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "rightmove_crawler"
