from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class SearchURL(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    url = models.URLField()


class Property(models.Model):
    search_url = models.ForeignKey(SearchURL, on_delete=models.CASCADE)
    property_url = models.URLField()
    price = models.IntegerField(null=True, blank=True)
    property_type = models.CharField(max_length=250, null=True, blank=True)
    address = models.CharField(max_length=250, null=True, blank=True)
    agent_url = models.URLField(null=True, blank=True)
    postcode = models.CharField(max_length=10, null=True, blank=True)
    number_bedrooms = models.IntegerField(null=True, blank=True)
    search_date = models.DateTimeField(null=True, blank=True)
